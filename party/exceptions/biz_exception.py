# -*- coding: utf-8 -*-

# 自定义业务异常类
class Bizexception(Exception):
 
    def __init__(self, code,args=("业务异常")):
        self.args = args
        self.code = code