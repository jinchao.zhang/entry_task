drop table task.`party_chnl_info_tab`;
CREATE TABLE task.`party_chnl_info_tab` (
  `chnl_id` varchar(2) COLLATE utf8mb4_bin NOT NULL,
  `chnl_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `chnl_desc` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL,
  `creat_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`chnl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

drop table task.`party_comments_tab`;
CREATE TABLE task.`party_comments_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `comment_content` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `creat_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

drop table task.`party_images_tab`;
CREATE TABLE task.`party_images_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `party_id` int(11) NOT NULL,
  `photo_url` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `creat_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

drop table task.`party_info_tab`;
CREATE TABLE task.`party_info_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `party_name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `party_chnl_id` varchar(2) COLLATE utf8mb4_bin NOT NULL,
  `party_chnl_name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `party_pos` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `party_url` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `party_desc` varchar(300) COLLATE utf8mb4_bin NOT NULL,
  `creat_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

drop table task.`party_user_tab`;
CREATE TABLE task.`party_user_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `party_id` int(11) NOT NULL,
  `creat_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

drop table task.`user_info_tab`;
CREATE TABLE task.`user_info_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_bin NOT NULL,
  `photo_url` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `creat_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;