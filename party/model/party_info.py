# -*- coding: utf-8 -*-
"""
活动管理model
"""
from django.db import models
from user_info import UserInfo

# Create your models here.
# 活动model
class PartyInfo(models.Model):
    id = models.AutoField(primary_key=True)
    party_name = models.CharField(max_length=50)
    party_chnl_id = models.CharField(max_length=2)
    party_chnl_name = models.CharField(max_length=50)
    begin_time = models.DateTimeField()
    end_time = models.DateTimeField()
    party_pos = models.CharField(max_length=100)
    party_url = models.CharField(max_length=200)
    party_desc = models.CharField(max_length=300)
    creat_time = models.DateTimeField()
    create_user = models.ForeignKey(UserInfo)
    update_time = models.DateTimeField()
    update_user_id = models.IntegerField()
    class Meta:
        db_table='party_info_tab'

# 活动明细图片model
class PartyImage(models.Model):
    id = models.AutoField(primary_key=True)
    #party_id = models.IntegerField()
    photo_url = models.CharField(max_length=200)
    party = models.ForeignKey(to="PartyInfo",to_field="id",related_name="partyImage")
    creat_time = models.DateTimeField()
    create_user = models.ForeignKey(UserInfo)
    update_time = models.DateTimeField()
    update_user_id = models.IntegerField()
    class Meta:
        db_table='party_images_tab'

# 活动参与用户model
class PartyUser(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(UserInfo)
    party = models.ForeignKey(to="PartyInfo",to_field="id",related_name="partyUser")
    creat_time = models.DateTimeField()
    create_user = models.ForeignKey(UserInfo)
    update_time = models.DateTimeField()
    update_user_id = models.IntegerField()
    class Meta:
        db_table='party_user_tab'

# 活动评论model
class PartyComments(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(UserInfo)
    party_id = models.IntegerField()
    comment_content = models.CharField(max_length=200)
    creat_time = models.DateTimeField()
    create_user = models.ForeignKey(UserInfo)
    update_time = models.DateTimeField()
    update_user_id = models.IntegerField()
    class Meta:
        db_table='party_comments_tab'

# 活动类别model
class PartyChnlInfo(models.Model):
    chnl_id = models.CharField(primary_key=True,max_length=2)
    chnl_name = models.CharField(max_length=50)
    chnl_desc = models.CharField(max_length=200)
    creat_time = models.DateTimeField()
    create_user_id = models.IntegerField()
    update_time = models.DateTimeField()
    update_user_id = models.IntegerField()
    class Meta:
        db_table='party_chnl_info_tab'
