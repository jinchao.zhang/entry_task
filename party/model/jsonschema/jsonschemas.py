# -*- coding: utf-8 -*-
"""
jsonschema 管理
"""

# 发布评论jsonschema
pub_comm_json={
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "pub comment",
    "description": "pub comment",
    "type": "object",
    "properties": {
        "id": {
            "type": "string",
            "minLength": 1
        },
        "pub_c": {
            "type": "string",
            "minLength": 1
        }
    },
    "required": ["id", "pub_c"]
}