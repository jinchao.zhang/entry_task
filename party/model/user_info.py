# -*- coding: utf-8 -*-
"""
用户管理model
"""
from django.db import models

# Create your models here.
# 用户model
class UserInfo(models.Model):
    id = models.AutoField(u'id',primary_key=True)
    user_name = models.CharField(u'用户名',max_length=50)
    password = models.CharField(u'密码',max_length=20)
    email = models.CharField(u'邮箱',max_length=30)
    photo_url = models.CharField(u'头像',max_length=200)
    creat_time = models.DateTimeField(u'创建时间',null=True, blank=True)
    create_user_id = models.IntegerField(u'创建人',null=True, blank=True)
    update_time = models.DateTimeField(u'更新时间',null=True, blank=True)
    update_user_id = models.IntegerField(u'更新人',null=True, blank=True)
    def __unicode__(self):
        return self.user_name
    class Meta:
        db_table='user_info_tab'