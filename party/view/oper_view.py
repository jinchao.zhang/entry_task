# -*- coding: utf-8 -*-
"""
运营后管视图
"""
from django.shortcuts import render
from party.manager import login_manager
from party.manager import party_manager
from django.http import HttpResponse
from party.exceptions.biz_exception import Bizexception
import json
from django.core import serializers
from django.core.paginator import Paginator
from ..common.contants import Const
from ..model.party_info import PartyInfo
from ..model.user_info import UserInfo
from ..common.dateutil import formatDate
import datetime
from party import tasks
from django.db import transaction

# Create your views here.
# 查询未结束活动列表
def list_party(request):
    pageno = request.GET.get('pageno',None)
    partylist = party_manager.party_enable_all_list(pageno,None,None,None)
    data = {
        'retcode': 0,
        'message': 'success',
        'data':{
            # 'total':pages.count,
            'pageno':pageno,
            'count”':Const.PAGE_SIZE,
            'list':toDicArray(partylist)
        }
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#发送邮件
def triger_task(request):
    id = request.GET.get('id',None)
    res = tasks.send_mail.delay(id)
    data = {
        'retcode': 0,
        'message': 'success',
        'data':{
            'task_id' : res.task_id
        }
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#查询活动类别列表
def chnl_list(request):
    pageno = request.GET.get('pageno',None)
    pages,chnllist=party_manager.list_chnl(pageno)
    data = {
        'retcode': 0,
        'message': 'success',
        'data':{
            'total':pages.count,
            'pageno':pageno,
            'count”':Const.PAGE_SIZE,
            'list':toDicChnlArray(chnllist)
        }
    }

    return HttpResponse(json.dumps(data), content_type='application/json')

#查询所有活动列表
def party_list(request):
    pageno = request.GET.get('pageno',None)
    pages,partylist = party_manager.party_all_list(pageno)
    data = {
        'retcode': 0,
        'message': 'success',
        'data':{
            'total':pages.count,
            'pageno':pageno,
            'count”':Const.PAGE_SIZE,
            'list':toDicArray(partylist)
        }
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#查询活动明细
def party_view(request):
    id = request.GET.get('id',None)
    party = party_manager.view(id)
    data = {
        'retcode': 0,
        'message': 'success',
        'data':toDicPartyView(party)
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#保存活动
@transaction.atomic
def party_save(request):
    user = request.session.get('session_user',default=None)
    _Post = request.POST
    party_manager.save_party(_Post.dict(),user)
    data = {
        'retcode': 0,
        'message': 'success'
    }
    return HttpResponse(json.dumps(data), content_type='application/json')
#保存活动类别
def chnl_save(request):
    user = request.session.get('session_user',default=None)
    _Post = request.POST
    user_id = _Post.get('user_id',None)
    if not user_id:
        user_id = user['id']
    post_data = {
        'chnl_id' : _Post.get('chnl_id',None),
        'chnl_name' : _Post.get('chnl_name',None),
        'chnl_desc' : _Post.get('chnl_desc',None),
        'creat_time' : datetime.datetime.now(),
        'create_user_id':user_id,
        'update_time':datetime.datetime.now(),
        'update_user_id' : user_id,
    }
    party_manager.save_chnl(post_data)
    data = {
        'retcode': 0,
        'message': 'success'
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#活动列表转字典数组
def toDicArray(partylist):
    diclist = []
    for party in partylist:
        creat_user = party.create_user
        dic = {
            'id' : party.id,
            'party_name' : party.party_name,
            'party_chnl_id' : party.party_chnl_id,
            'party_chnl_name': party.party_chnl_name,
            'begin_time' : formatDate(party.begin_time),
            'end_time' : formatDate(party.end_time),
            'party_pos' : party.party_pos,
            'party_url' : party.party_url,
            'party_desc' : party.party_desc,
            'creat_time' : formatDate(party.creat_time),
            'create_user_id' : creat_user.id,
            'create_user_name' : creat_user.user_name,
            'create_user_photo' : creat_user.photo_url
        }
        diclist.append(dic)
    return diclist

#活动类别列表转字典数组
def toDicChnlArray(chnllist):
    diclist = []
    for chnl in chnllist:
        dic = {
            'chnl_id' : chnl.chnl_id,
            'chnl_name' : chnl.chnl_name,
            'chnl_desc' : chnl.chnl_desc,
            'creat_time' : formatDate(chnl.creat_time),
            'create_user_id':chnl.create_user_id,
            'update_time':formatDate(chnl.update_time),
            'update_user_id' : chnl.update_user_id,
        }
        diclist.append(dic)
    return diclist

#活动明细转字典
def toDicPartyView(party):
    
    dic = {
        'id' : party.id,
        'party_name' : party.party_name,
        'party_chnl_id' : party.party_chnl_id,
        'party_chnl_name': party.party_chnl_name,
        'begin_time' : formatDate(party.begin_time),
        'end_time' : formatDate(party.end_time),
        'party_pos' : party.party_pos,
        'party_url' : party.party_url,
        'party_desc' : party.party_desc
    }
    partyimages = []
    imglist = party.partyImage.all()
    for img in imglist:
        partyimages.append(img.photo_url)
    dic['photo_details'] = ','.join(partyimages)
    
    return dic

#ab test保存用户
def test_user(request):        
    _Post = request.POST
    post_data = {
        'user_name' : _Post.get('user_name',None),
        'password' : _Post.get('password',None),
        'email' : _Post.get('email',None),
        'photo_url' : _Post.get('photo_url',None)
    }
    login_manager.save_user(post_data)
    data = {
        'retcode': 0,
        'message': 'success'
    }
    return HttpResponse(json.dumps(data), content_type='application/json')