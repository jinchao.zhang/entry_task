# -*- coding: utf-8 -*-
"""
前端视图管理
"""
from django.shortcuts import render
from party.manager import login_manager
from party.manager import party_manager
from django.http import HttpResponse
from party.exceptions.biz_exception import Bizexception
import json
from django.core import serializers
from django.core.paginator import Paginator
from ..common.contants import Const
from ..model.party_info import PartyInfo
from ..model.user_info import UserInfo
from ..common.dateutil import formatDate
from party.common.json_checker import check_json
from jsonschema.validators import Draft4Validator
from party.model.jsonschema import jsonschemas


# Create your views here.
#前端跳转登陆
def toLogin(request):
    return render(request,'login.html')

#前端用户登陆
def login(request):
    username = request.POST.get('user_name',None)
    password = request.POST.get('password',None)
    try:
        user = login_manager.login(username,password)
        # 用户信息保存session中
        request.session['session_user'] = {
            'id':user.id,
            'user_name':user.user_name
        }
    except Bizexception as e:
        return render(request,'login.html',{'errorcode':e.code})
    else:
        return render(request,'party_list.html',{'user':user})
#查询未结束活动列表
def party_enable_all_list(request):
    pageno = request.POST.get('pageno',None)
    party_type = request.POST.get('party_type',None)
    startdate = request.POST.get('startdate',None)
    enddate = request.POST.get('enddate',None)
    # pages,partylist = party_manager.party_enable_all_list(pageno,party_type,startdate,enddate)
    partylist = party_manager.party_enable_all_list(pageno,party_type,startdate,enddate)
    data = {
        'retcode': 0,
        'message': 'success',
        'data':{
            # 'total':pages.count,
            'pageno':pageno,
            'count”':Const.PAGE_SIZE,
            'list':toDicArray(partylist)
        }
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

# 查询我参加的活动列表
def list_myparty(request):
    pageno = request.GET.get('pageno',1)
    user = request.session.get('session_user',default=None)
    pages,party_user_list = party_manager.list_myparty(pageno,user['id'])
    data = {
        'retcode': 0,
        'message': 'success',
        'data':{
            'total':pages.count,
            'pageno':pageno,
            'count”':Const.PAGE_SIZE,
            'list':toDicUserPartyArray(party_user_list)
        }
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

# 查询活动明细
def party_view(request):
    id = request.GET.get('id',None)
    party = party_manager.view(id)
    data = {
        'retcode': 0,
        'message': 'success',
        'data':toDicView(party,request.session.get('session_user',default=None))
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#取消加入活动
def cancel_join(request):
    id = request.GET.get('id',None)
    user = request.session.get('session_user',default=None)
    print user
    party_manager.join(id,user['id'],False)
    data = {
        'retcode': 0,
        'message': 'success'
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#加入活动
def join(request):
    id = request.GET.get('id',None)
    user = request.session.get('session_user',default=None)
    party_manager.join(id,user['id'],True)
    data = {
        'retcode': 0,
        'message': 'success'
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#查询屏录列表
def list_comment(request):
    id = request.GET.get('id',None)
    pageno = request.GET.get('pageno',None)
    pages,commentlist = party_manager.list_comment(id,pageno)
    data = {
        'retcode': 0,
        'message': 'success',
        'data':{
            'total':pages.count,
            'pageno':pageno,
            'count”':Const.PAGE_SIZE,
            'list':toDicCommentArray(commentlist)
        }
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#发布评论
@check_json(validator = Draft4Validator(schema=jsonschemas.pub_comm_json))
def pub_comment(request):
    id = request.POST.get('id',None)
    pub_c = request.POST.get('pub_c',None)
    user = request.session.get('session_user',default=None)
    party_manager.pub_comment(id,user['id'],pub_c)
    data = {
        'retcode': 0,
        'message': 'success'
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

#未结束活动转字典数组
def toDicArray(partylist):
    diclist = []
    for party in partylist:
        creat_user = party.create_user
        dic = {
            'id' : party.id,
            'party_name' : party.party_name,
            'party_chnl_id' : party.party_chnl_id,
            'party_chnl_name': party.party_chnl_name,
            'begin_time' : formatDate(party.begin_time),
            'end_time' : formatDate(party.end_time),
            # 'party_pos' : party.party_pos,
            'party_url' : party.party_url,
            'party_desc' : party.party_desc,
            # 'creat_time' : formatDate(party.creat_time),
            'create_user_id' : creat_user.id,
            'create_user_name' : creat_user.user_name,
            'create_user_photo' : creat_user.photo_url
        }
        diclist.append(dic)
    return diclist

#活动明细转字典
def toDicView(party,user):
    print user
    creat_user = party.create_user
    dic = {
        'id' : party.id,
        'party_name' : party.party_name,
        'party_chnl_id' : party.party_chnl_id,
        'party_chnl_name': party.party_chnl_name,
        'begin_time' : formatDate(party.begin_time),
        'end_time' : formatDate(party.end_time),
        'party_pos' : party.party_pos,
        'party_url' : party.party_url,
        'party_desc' : party.party_desc,
        'creat_time' : formatDate(party.creat_time),
        'create_user_id' : creat_user.id,
        'create_user_name' : creat_user.user_name,
        'create_user_photo' : creat_user.photo_url,
        'user_join':'Y'
    }
    partyimages = []
    imglist = party.partyImage.all()
    for img in imglist:
        partyimages.append(img.photo_url)
    dic['photo_urls'] = partyimages
    partyusers = []
    userlist = party.partyUser.all().select_related('user')
    for party_user in userlist:
        partyusers.append(party_user.user.user_name)
    dic['party_users'] = partyusers
    print partyusers
    if user['user_name'] not in partyusers:
        dic['user_join'] = 'N'
    return dic

#活动参加用户转字典数组
def toDicUserPartyArray(party_user_list):
    diclist = []
    for party_user in party_user_list:
        party = party_user.party
        creat_user = party.create_user
        dic = {
            'id' : party.id,
            'party_name' : party.party_name,
            'party_chnl_id' : party.party_chnl_id,
            'party_chnl_name': party.party_chnl_name,
            'begin_time' : formatDate(party.begin_time),
            'end_time' : formatDate(party.end_time),
            'party_pos' : party.party_pos,
            'party_url' : party.party_url,
            'party_desc' : party.party_desc,
            'creat_time' : formatDate(party.creat_time),
            'create_user_id' : creat_user.id,
            'create_user_name' : creat_user.user_name,
            'create_user_photo' : creat_user.photo_url
        }
        diclist.append(dic)
    return diclist

#活动评论转字典数组
def toDicCommentArray(commentlist):
    diclist = []
    for comment in commentlist:
        pubuser = comment.user
        dic = {
            'id' : comment.id,
            'pub_user_name' : pubuser.user_name,
            'pub_user_id' : pubuser.id,
            'comment_content': comment.comment_content,
            'pub_time' : formatDate(comment.creat_time)
        }
        diclist.append(dic)
    return diclist