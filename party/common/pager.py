# -*- coding: utf-8 -*-
"""
分页工具
"""
from django.core.paginator import Paginator
from contants import Const

"""get the paginator"""
def getPages(pageno, objectlist):
    if not pageno:
        pageno = 1   
    paginator = Paginator(objectlist, Const.PAGE_SIZE)
    start = int(pageno)*Const.PAGE_SIZE - Const.PAGE_SIZE
    objectlist = objectlist[start:Const.PAGE_SIZE]
 
    return paginator, objectlist