# -*- coding: utf-8 -*-
"""
日期工具
"""
import datetime
from datetime import date

"""format date """
def formatDate(dateobj):
    if not dateobj:
        return ""
    if isinstance(dateobj, datetime.datetime):
        return dateobj.strftime('%Y-%m-%d %H:%M:%S')
    elif isinstance(dateobj, date):
        return dateobj.strftime('%Y-%m-%d')

    return dateobj
""" 日期相加 """
def dateAdd(date_obj,day_num):
    delta=datetime.timedelta(days=day_num)
    return date_obj+delta

# 字符串转日期 %Y-%m-%d
def parseDate(date_str):
    return datetime.datetime.strptime(date_str,'%Y-%m-%d')

# 字符串转日期时间 %Y-%m-%d %H:%M:%S
def parseDateTime(date_str):
    return datetime.datetime.strptime(date_str,'%Y-%m-%d %H:%M:%S')