# -*- coding: utf-8 -*-
"""
json数据验证工具
"""
from functools import wraps
from jsonschema.exceptions import ValidationError
from django.http import HttpResponse
import json

# 验证json
def check_json(validator):
    def decorated(f):
        @wraps(f)
        def wrapper(*args,**kwargs):
            data = args[0].POST
            try:
                validator.validate(data)
            except ValidationError as ex:
                # data = {
                #     'retcode': 99,
                #     'message': ex.message
                # }
                # return HttpResponse(json.dumps(data), content_type='application/json')
                raise ex
            else:
                return f(*args,**kwargs)
        return wrapper
    return decorated