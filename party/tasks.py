# -*- coding: utf-8 -*-
"""
celery 任务管理
"""
from celery import task
from party.manager import party_manager

# 发送邮件
@task
def send_mail(id):
    party_manager.send_mail(id)