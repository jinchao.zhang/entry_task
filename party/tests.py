# -*- coding: utf-8 -*-
"""
单元测试用例
"""
from django.test import TestCase
import unittest
from django.test import Client
import datetime
import thread
import time

# Create your tests here.

max_type_count = 1000

# 初始化活动类别
class TestType(unittest.TestCase):
    # 保存
    def test_save(self):
        count = 0
        for num in range(1,9):
            count = cirle_insert(num,count)
            if count > max_type_count:
                return
        for num in range(65,90):
            count = cirle_insert(num,count)
            if count > max_type_count:
                return
        for num in range(97,122):
            count = cirle_insert(num,count)
            if count > max_type_count:
                return    

#根据0-9 A-Z a-z 作为id插入
def cirle_insert(num,count):
    for n_num in range(1,9):
        if count > max_type_count:
            return count
        insert(num,n_num)
        count += 1
    for n_num in range(65,90):
        if count > max_type_count:
            return count
        insert(num,n_num)
        count += 1
    for n_num in range(97,122):
        if count > max_type_count:
            return count
        
        insert(num,n_num)
        count += 1
    return count
        

#插入活动类别
def insert(num,n_num):
    c_num = num
    if num > 10:
        c_num = chr(num)
    c_n_num = n_num
    if c_n_num > 10:
        c_n_num =  chr(n_num)
    id = str(c_num)+str(c_n_num)
    post_data = {
        'chnl_id' : id,
        'chnl_name' : '类别'+id,
        'chnl_desc' : '类别'+id,
        'creat_time' : datetime.datetime.now(),
        'user_id':1
    }

    c = Client()
    # party_manager.save_chnl(post_data)
    c.post('/oper/oper_chnl_save', post_data)#请求保存类别url

#保存用户
class TestUser(unittest.TestCase):
    def test_save(self):
        for num in range(10000):
            post_data ={
                'user_name': 'name' + str(num),
                'password': '123',
                'email': 'jinchao.zhang@shopee.com',
                'photo_url': 'www.baidu.com'
            }
            c = Client()
            c.post('/oper/test_user',post_data) #请求保存用户url

#保存活动
class TestAct(unittest.TestCase):
    def test_save(self):
        #开启20个线程并发保存
        for num in range(60,69):
            thread.start_new_thread( insertAct, ("a"+str(num), ) )
        time.sleep(12000)
        
#插入活动
def insertAct(seq):
    for num in range(500):
        post_data ={
            'id':'',
            'party_chnl_id': '11',
            'party_name': '活动'+str(seq)+'-'+str(num),
            'begin_time': '2019-03-29 08:00:00',
            'end_time': '2019-06-29 18:00:00',
            'party_pos': '小树林',
            'party_url': '主图',
            'party_desc': '描述1111',
            'user_id': 1,
            # 'photo_details': 'p11',
        }
        c = Client()
        c.post('/oper/oper_party_save',post_data) #请求保存活动url