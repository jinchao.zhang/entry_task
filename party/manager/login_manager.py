# -*- coding: utf-8 -*-
"""
登陆管理业务
"""
from ..model.user_info import UserInfo
from ..exceptions.biz_exception import Bizexception

# 用户登陆
def login(username,password):
    if not username.strip() or not password.strip():
        raise Bizexception("用户名或密码不能为空！")
    try:
        user = UserInfo.objects.get(user_name=username)
    except UserInfo.DoesNotExist:
        raise Bizexception("用户不存在！")
    else:
        if user.password != password:
            raise Bizexception("用户名或密码错误！")
        return user

# ab tet 初始用户
def save_user(margs):

    user = UserInfo(user_name = margs['user_name'],password = margs['password'],\
        email = margs['email'],photo_url = margs['photo_url'])
    user.save()