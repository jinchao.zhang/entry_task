# -*- coding: utf-8 -*-
"""
活动管理业务
"""
from ..model.party_info import PartyInfo
from ..model.party_info import PartyChnlInfo
from ..exceptions.biz_exception import Bizexception
from ..common.contants import Const
from django.core.paginator import Paginator
from ..common.pager import getPages
from ..model.user_info import UserInfo
from ..model.party_info import PartyUser
from ..model.party_info import PartyComments
from ..model.party_info import PartyImage
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from ..common.dateutil import formatDate
from ..common.dateutil import dateAdd
from ..common.dateutil import parseDate
from ..common.dateutil import parseDateTime
import datetime
from django.utils import timezone

# 查询未结束活动列表
def party_enable_all_list(pageno,party_type,startdate,enddate):
    
    try:
        # now = datetime.datetime.now()
        now = timezone.now()
        partylist = PartyInfo.objects.filter(end_time__gte=now)
        if party_type:
            partylist = partylist.filter(party_chnl_id=party_type)
        if startdate:
            partylist = partylist.filter(begin_time__gte=parseDate(startdate))
        if enddate:
            partylist = partylist.filter(end_time__lt=dateAdd(parseDate(enddate),1))
        
        partylist = partylist.select_related('create_user').defer("party_pos","creat_time","update_time","update_user_id",\
            "create_user__password","create_user__email","create_user__creat_time","create_user__create_user_id",\
            "create_user__update_time","create_user__update_user_id")
        # pages,partylist = getPages(pageno,partylist)
        if not pageno:
            pageno = 1
        
        start = int(pageno)*Const.PAGE_SIZE - Const.PAGE_SIZE
        partylist = partylist[start:Const.PAGE_SIZE]
    except PartyInfo.DoesNotExist:
        raise Bizexception("查无记录！")
    else:
        # return pages,partylist
        return partylist

# 查询所有活动列表
def party_all_list(pageno):
    try:
        partylist = PartyInfo.objects.all().select_related('create_user')
        pages,partylist = getPages(pageno,partylist)
    except PartyInfo.DoesNotExist:
        raise Bizexception("查无记录！")
    else:
        return pages,partylist

# 查询我参与的活动列表
def list_myparty(pageno,user_id):
    party_user_list = PartyUser.objects.filter(user_id=user_id).select_related('party')
    pages,party_user_list = getPages(pageno,party_user_list)
    return pages,party_user_list

# 查询活动明细
def view(id):
    try:
        party = PartyInfo.objects.select_related('create_user').get(id=id)
    except PartyInfo.DoesNotExist:
        raise Bizexception("记录不存在！")
    else:
        return party

# 参与或取消参与活动
def join(id,user_id,join_flag):
    userlist = PartyUser.objects.filter(party_id=id,user_id=user_id)
    if join_flag:
        if userlist:
            raise Bizexception("用户不能重复加入！")
        PartyUser.objects.create(user_id=user_id,party_id=id)
    else:
        if not userlist:
            raise Bizexception("用户无加入记录！")
        PartyUser.objects.filter(user_id=user_id,party_id=id).delete()

# 查询活动评论明细
def list_comment(id, pageno):

        commentlist = PartyComments.objects.filter(party_id=id).select_related('user').order_by('-creat_time')
        pages,commentlist = getPages(pageno,commentlist)
        return pages,commentlist

# 查询活动类别列表
def list_chnl(pageno):
        chnllist = PartyChnlInfo.objects.all()
        pages,chnllist = getPages(pageno,chnllist)
        return pages,chnllist

# 发布活动评论
def pub_comment(id,user_id,pub_c):
    PartyComments.objects.create(user_id=user_id,party_id=id,\
        comment_content=pub_c,creat_time=datetime.datetime.now())

# 发送邮件
def send_mail(id):
    party = view(id)
    userlist = party.partyUser.all().select_related('user')
    from_email = settings.DEFAULT_FROM_EMAIL
    subject = '聚会邀请'
    content = '''聚会活动【{}】，请大家准时前往目的地集合!
    时间：{}-{},
    地点：{},
    活动详情：{}
    '''
    content = content.format(party.party_name,formatDate(party.begin_time),formatDate(party.end_time),party.party_pos,party.party_desc)
    to_addr = []
    for party_user in userlist:
        to_addr.append(party_user.user.email)

    print to_addr
    # subject 主题 content 内容 to_addr 是一个列表，发送给哪些人
    msg = EmailMultiAlternatives(subject, content,from_email,to_addr)
    
    msg.content_subtype = "html"
   
    # 发送
    msg.send()

# 保存活动类别
def save_chnl(margs):
    pc = PartyChnlInfo(chnl_id = margs['chnl_id'],chnl_name = margs['chnl_name'],\
        chnl_desc = margs['chnl_desc'],creat_time = margs['creat_time'],\
        create_user_id = margs['create_user_id'],update_time = margs['update_time'],\
        update_user_id = margs['update_user_id'])
    pc.save()

# 保存活动
def save_party(party_info,user):
    # chnl_id = party_info.get('party_chnl_id',None)
    # pc = PartyChnlInfo.objects.get(chnl_id=chnl_id)
    # id=party_info.get('id',None)
    # party_chnl_name=pc.chnl_name
    # party_name=party_info.get('party_name',None)
    # begin_time=parseDateTime(party_info.get('begin_time',None))
    # end_time=parseDateTime(party_info.get('end_time',None))
    # party_pos=party_info.get('party_pos',None)
    # party_url=party_info.get('party_url',None)
    # party_desc=party_info.get('party_desc',None)
    chnl_id = party_info['party_chnl_id']
    pc = PartyChnlInfo.objects.get(chnl_id=chnl_id)
    id=party_info['id']
    party_chnl_name=pc.chnl_name
    party_name=party_info['party_name']
    begin_time=parseDateTime(party_info['begin_time'])
    end_time=parseDateTime(party_info['end_time'])
    party_pos=party_info['party_pos']
    party_url=party_info['party_url']
    party_desc=party_info['party_desc']
    user_id = None
    if not user:
        user_id = party_info['user_id']
    else:
       user_id = user['id']
    if id:
        pu = PartyInfo.objects.get(id=id)
        pu.update_time = datetime.datetime.now()
        pu.update_user_id = user_id
        pu.party_name = party_name
        pu.party_chnl_name = party_chnl_name
        pu.party_chnl_id = chnl_id
        pu.begin_time = begin_time
        pu.end_time = end_time
        pu.party_pos = party_pos
        pu.party_url = party_url
        pu.party_desc = party_desc
        pu.save()
    else :
        pi = PartyInfo.objects.create(party_name=party_name,party_chnl_id=chnl_id,party_chnl_name=party_chnl_name,begin_time=begin_time,end_time=end_time,\
            party_pos=party_pos,party_url=party_url,party_desc=party_desc,create_user_id=user_id,creat_time=datetime.datetime.now())
        id = pi.id
   
    partyimages = party_info.get('photo_details',None)
    if not partyimages :
        return
    pohto_urls = partyimages.split(',') 
    PartyImage.objects.filter(party_id=id).delete()
    for img in pohto_urls:
       PartyImage.objects.create(party_id=id,photo_url=img,creat_time=datetime.datetime.now(),create_user_id=user_id)
