# -*- coding: utf-8 -*-
"""
url路由
entry_task/* 前端api接口
oper/* 运营后台api接口
admin/* 管理后台api接口
"""
from django.conf.urls import include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
 
from party.view import views
from party.view import oper_view
 
urlpatterns = [
    url(r'^$', views.toLogin),                                                  #前端跳转登陆url
    url(r'^entry_task/login$', views.login),                                    #前端登陆url
    url(r'^entry_task/party_enable_all_list$',views.party_enable_all_list),     #前端查询未结束活动列表url
    url(r'^entry_task/list_myparty$',views.list_myparty),                       #前端查询我参加的活动url
    url(r'^entry_task/party_view$',views.party_view),                           #前端活动详情url
    url(r'^entry_task/join$',views.join),                                       #前端加入活动url
    url(r'^entry_task/cancel_join$',views.cancel_join),                         #前端取消加入活动url
    url(r'^entry_task/pub_comment$',views.pub_comment),                         #前端发布评论url
    url(r'^entry_task/list_comment$',views.list_comment),                       #前端查询评论url
    
    url(r'^oper/party_list$',oper_view.list_party),                             #运营查询未结束活动列表url
    url(r'^oper/triger_task$',oper_view.triger_task),                           #运营触犯邮件发送任务url
    url(r'^oper/oper_chnl_save$',oper_view.chnl_save),                          #运营保存活动类别url
    url(r'^oper/oper_chnl_list$',oper_view.chnl_list),                          #运营查询活动类别url
    url(r'^oper/oper_party_list$',oper_view.party_list),                        #运营查询活动url
    url(r'^oper/oper_party_view$',oper_view.party_view),                        #运营查询活动明细url
    url(r'^oper/oper_party_save$',oper_view.party_save),                        #运营保存活动url

    url(r'^oper/test_user$',oper_view.test_user),                               #ab test初始化用户数据

    url(r'^admin/', include(admin.site.urls)),                                  #管理后台url
]

# ... the rest of your URLconf goes here ...
urlpatterns += staticfiles_urlpatterns()